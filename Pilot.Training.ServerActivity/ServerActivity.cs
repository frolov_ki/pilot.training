﻿using Ascon.Pilot.DataClasses;
using Ascon.Pilot.DataModifier;
using Ascon.Pilot.ServerExtensions.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Threading.Tasks;

namespace Pilot.Training.ServerActivity
{
    [Export(typeof(IServerActivity))]
    public class ServerActivity : IServerActivity
    {
        public string Name => "DoServerActivity";

        public Task RunAsync(IModifierBase modifier,
            IModifierBackend backend,
            IServerActivityContext serverActivityContext,
            IAutomationEventContext automationEventContext)
        {
            var parameters = serverActivityContext.Params;
            var source = automationEventContext.Source;
            var metadata = backend.GetMetadata();
            var security = backend.GetObjectSecurity();
            var accerrRecords = security.GetAccessRecordsWithInheritable(source, (new List<INChange>(automationEventContext.Changes)).AsReadOnly());
            var person = automationEventContext.InitiatingPerson;
            
            var accessSubject  = AccessSubject.ByPerson(person);
            var access = security.CalcAccess(source, accessSubject);
            
            var projectValue = source.Attributes["project"];
            if (!(projectValue.Value is string[] projectValueArray))
                return Task.CompletedTask;

           if (!Guid.TryParse( projectValueArray[0], out var projectId))
                return Task.CompletedTask;
            var obj = backend.GetObject(projectId);
            var builder = modifier.EditObject(obj);
            builder.SetAttribute("attributFromServerActivity",  Guid.NewGuid().ToString());
            return Task.CompletedTask;
        }
    }
}
