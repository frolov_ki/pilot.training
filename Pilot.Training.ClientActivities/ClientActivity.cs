﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Automation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training.ClientActivities
{
    [Export(typeof(IAutomationActivity))]
    public class ClientActivity : AutomationActivity
    {
        private readonly IObjectsRepository _objectsRepository;

        [ImportingConstructor]
        public ClientActivity(IObjectsRepository objectsRepository)
        {
            _objectsRepository = objectsRepository;
        }

        public override string Name => "DoClientActivity";

        public override Task RunAsync(IObjectModifier modifier, IAutomationBackend backend, IAutomationEventContext context, TriggerType triggerType)
        {
            var obj = context.Source;
            var builder = modifier.EditById(obj.Id);
            builder.SetAttribute("name", "New name");
            var paramValue = Params["myParam"];
            //Errors.Clear();
            Errors.Add("DoClientActivity", "DoClientActivity error");
            //modifier.Apply();
            return Task.CompletedTask;
        }


    }
}
