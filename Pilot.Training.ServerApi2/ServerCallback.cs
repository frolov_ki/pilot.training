﻿using Ascon.Pilot.DataClasses;
using Ascon.Pilot.Server.Api.Contracts;
using Ascon.Pilot.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training.ServerApi2
{
    internal class ServerCallback : IServerCallback
    {
        public void NotifyAccessChangeset(Guid objectId)
        {
            
        }

        public void NotifyChangeAsyncCompleted(DChangeset changeset)
        {
            
        }

        public void NotifyChangeAsyncError(Guid identity, ProtoExceptionInfo exception)
        {
            
        }

        public void NotifyChangeset(DChangeset changeset)
        {
            
        }

        public void NotifyCommandResult(Guid requestId, byte[] data, ServerCommandResult result)
        {
            
        }

        public void NotifyCustomNotification(string name, byte[] data)
        {
            
        }

        public void NotifyDMetadataChangeset(DMetadataChangeset changeset)
        {
            
        }

        public void NotifyDNotificationChangeset(DNotificationChangeset changeset)
        {
            
        }

        public void NotifyGeometrySearchResult(DGeometrySearchResult searchResult)
        {
            
        }

        public void NotifyOrganisationUnitChangeset(OrganisationUnitChangeset changeset)
        {
            
        }

        public void NotifyPersonChangeset(PersonChangeset changeset)
        {
            
        }

        public void NotifySearchResult(DSearchResult searchResult)
        {
            
        }
    }
}
