﻿using Ascon.Pilot.DataClasses;
using Ascon.Pilot.Server.Api.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training.ServerApi2
{
    internal class MessageCallback : IMessageCallback
    {
        public void CreateNotification(DNotification notification)
        {
            
        }

        public void NotifyMessageCreated(NotifiableDMessage message)
        {
            
        }

        public void NotifyOffline(int personId)
        {
            
        }

        public void NotifyOnline(int personId)
        {
            
        }

        public void NotifyTypingMessage(Guid chatId, int personId)
        {
            
        }

        public void UpdateLastMessageDate(DateTime maxDate)
        {
            
        }
    }
}
