﻿using Ascon.Pilot.DataClasses;
using Ascon.Pilot.Server.Api.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training.ServerApi2
{
    internal class ServerAdminCallBack : IServerAdminCallback
    {
        public void NotifyConnectionCountFromServer(int licenseType, int licenseCount)
        {
            
        }

        public void NotifyDatabaseChanged(DatabaseChangeset changeset)
        {
            
        }

        public void NotifyDMetadataChanged(string databaseName, DMetadataChangeset changeset)
        {
            
        }

        public void NotifyIndexingStateChanged(string databaseName, ushort percent)
        {
            
        }

        public void NotifyLicenseChanged()
        {
            
        }

        public void NotifyOrganisationUnitChanged(string databaseName, OrganisationUnitChangeset changeset)
        {
            
        }

        public void NotifyPersonChanged(string databaseName, PersonChangeset changeset)
        {
            
        }

        public void NotifyReservationsChanged()
        {
            
        }

        public void NotifySessionsFromServer(int licenseType, int licenseCount, int sessionsCount, Guid licenseId)
        {
            
        }
    }
}
