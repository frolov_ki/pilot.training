﻿

using Ascon.Pilot.ClientCore.Search;
using Ascon.Pilot.Common;
using Ascon.Pilot.DataClasses;
using Ascon.Pilot.DataModifier;
using Ascon.Pilot.Server.Api;
using Ascon.Pilot.Server.Api.Contracts;
using Pilot.Training.ServerApi2;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;

var client = new HttpPilotClient("http://localhost:5545");
client.Connect(false);
var authApi = client.GetAuthenticationApi();
authApi.Login("pilot-bim_ru", "serverUser", "111".EncryptAes(), false, 90);
//authApi.LoginServerAdministrator("serverUser", "111".EncryptAes(), false);
var serverAdminApi = client.GetServerAdminApi(new ServerAdminCallBack());
//serverAdminApi.OpenDatabase("pilot-bim_ru");
var serverApi = client.GetServerApi(new ServerCallback());
var messageApi = client.GetMessagesApi(new MessageCallback());
var fileApi = client.GetFileArchiveApi();
serverApi.OpenDatabase();
var fileStorageProvider = new FileSystemStorageProvider("d:\\Tmp\\");
var changSetUploader = new ChangesetUploader(fileApi, fileStorageProvider, null);
var backend = new Backend(serverApi, messageApi, changSetUploader);
var modifier = new Modifier(backend);
SearchObject();
Console.ReadKey();




void SearchObject()
{
    var builder = QueryBuilderFactory.CreateEmptyQueryBuilder();
    builder.Must(ObjectFields.ObjectState.Be(ObjectState.Alive));
    builder.Must(AttributeFields.String("name").Be("Object from server api"));
    var searchDefenition = new DSearchDefinition
    {
        Id = Guid.NewGuid(),
        Request =
        {
            MaxResults = int.MaxValue,
            SearchString = builder.ToString(),
            SearchKind = SearchKind.Custom
        }
    };
    serverApi.AddSearch(searchDefenition);
}





void CreateObject()
{
    var ecmDocType = backend.GetType("ecm_doc");
    var project = backend.GetObject(new Guid("387b8cdb-65e2-48ba-a3cf-3645e71285b9"));
    var builder = modifier.CreateObject(Guid.NewGuid(), project.Id, ecmDocType.Id);
    builder.SetAttribute("name", "Object from server api");
    modifier.Apply();

}


void SetUserOrgUnit()
{
    var people = serverAdminApi.LoadPeople();
    var orgUnits = serverAdminApi.LoadOrganisationUnits();
    var user = people.First(x => x.Login == "UserFromApi");
    var orgUnit = orgUnits.FirstOrDefault(x => x.Kind == OrgUnitKind.Position && x.Person == -1);
    if (orgUnit == null)
        return;
    serverAdminApi.SetPeopleOnPosition(new DPersonOnPositionData { Person = user.Id, PositionId = orgUnit.Id });
}



void CreateUser()
{
    var id = serverAdminApi.LoadPeople().Select(x => x.Id).Max() + 1;
    serverAdminApi.CreatePerson(new DPerson
    {
        Login = "UserFromApi",
        DisplayName = "User from api",
        Id = id

    }, "111".EncryptAes());

}

void CreateServerAdmin()
{

    serverAdminApi.CreateServerAdministrator(new DServerAdministrator
    {
        Login = "UserFromApi",
        Id = Guid.NewGuid()
    }
    , "111".EncryptAes());
}

void CreateType()
{
    var metaData = serverAdminApi.GetMetadata(0);
    var newId = metaData.Types.Select(x => x.Id).Max() + 1;
    var sort = metaData.Types.Select(x => x.Sort).Max() + 1;
    var folderType = new MType
    {
        Id = newId,
        Name = "FolderFromServer",
        Title = "Папка",
        Kind = TypeKind.User,
        HasFiles = false,
        Attributes =
                {
                    new MAttribute
                    {
                        Name = "title",
                        Title = "Наименование",
                        Obligatory = true,
                        ShowInTree = true,
                        UniquenessType = AttributeUniquenessType.NotUnique,
                        Type = MAttrType.String,
                        DisplayHeight = 1,
                        DisplaySortOrder = 0,
                    },
                    new MAttribute
                    {
                        Name = "order",
                        Title = "Порядок",
                        Obligatory = false,
                        ShowInTree = false,

                        IsService = true,
                        UniquenessType = AttributeUniquenessType.NotUnique,
                        Type = MAttrType.Integer,
                        DisplayHeight = 1,
                        DisplaySortOrder = 1
                    },
                    new MAttribute
                    {
                        Name = "color",
                        Title = "Цвет",
                        Obligatory = false,
                        ShowInTree = false,

                        IsService = true,
                        UniquenessType = AttributeUniquenessType.NotUnique,
                        Type = MAttrType.String,
                        DisplayHeight = 1,
                        DisplaySortOrder = 2
                    },
                    new MAttribute
                    {
                        Name = "updated",
                        Title = "Дата изменения",
                        Obligatory = false,
                        ShowInTree = false,

                        IsService = true,
                        UniquenessType = AttributeUniquenessType.NotUnique,
                        Type = MAttrType.DateTime,
                        DisplayHeight = 1,
                        DisplaySortOrder = 0
                    },
                },
        Sort = sort
    };

    metaData.Types.Add(folderType);
    serverAdminApi.UpdateMetadata(metaData);
}
