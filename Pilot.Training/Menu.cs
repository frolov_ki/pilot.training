﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Data;
using Ascon.Pilot.SDK.Menu;
using Ascon.Pilot.SDK.Toolbar;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training
{
    [Export(typeof(IMenu<ObjectsViewContext>))]
    [Export(typeof(IToolbar<ObjectsViewContext>))]
    internal class Menu : IMenu<ObjectsViewContext>, IToolbar<ObjectsViewContext>
    {
        private readonly IObjectsRepository _objectsRepository;
        private readonly IObjectModifier _objectModifier;
        private readonly IFileProvider _fileProvider;
        private readonly ISearchService _searchService;
        private readonly IMessagesRepository _messagesRepository;
        private readonly IMessageBuilder _messageBuilder;
        private const string MNUCREATE = "createMnu";
        private const string MNUNEDIT = "editMnu";
        private const string MNUNGETFILE = "getFileMnu";
        private const string MNUSEARCH = "searchMnu";
        private const string MNULOADMESSAGES = "loadMessagesMnu";
        private const string MNUSENDMESSAGES = "sendMessagesMnu";
        private const string MNUADDFILE = "addFileMnu";

        [ImportingConstructor]
        public Menu(
            IObjectsRepository objectsRepository,
            IObjectModifier objectModifier,
            IFileProvider fileProvider,
            ISearchService searchService,
            IMessagesRepository messagesRepository,
            IMessageBuilder messageBuilder)
        {
            _objectsRepository = objectsRepository;
            _objectModifier = objectModifier;
            _fileProvider = fileProvider;
            _searchService = searchService;
            _messagesRepository = messagesRepository;
            _messageBuilder = messageBuilder;
        }

        public void Build(IMenuBuilder builder, ObjectsViewContext context)
        {
            var item = builder
                .ReplaceItem("miSend")
                .WithHeader("Отправить")
                .WithIsEnabled(false);


            builder.AddItem(MNUCREATE, 0).WithHeader("Создать ecm_doc");
            builder.AddItem(MNUNEDIT, 0).WithHeader("Редактировать ecm_doc");
            builder.AddItem(MNUNGETFILE, 0).WithHeader("Получить файл");
            builder.AddItem(MNUSEARCH, 0).WithHeader("Поиск");
            builder.AddItem(MNULOADMESSAGES, 0).WithHeader("Подгрузка сообщений");
            builder.AddItem(MNUSENDMESSAGES, 0).WithHeader("Отправка сообщений");
            builder.AddItem(MNUADDFILE, 0).WithHeader("Добавить файл");
        }

        public async void OnMenuItemClick(string name, ObjectsViewContext context)
        {
            switch (name)
            {
                case MNUCREATE:
                    Create(context);
                    break;
                case MNUNEDIT:
                    Edit(context);
                    break;
                case MNUNGETFILE:
                    GetFile(context);
                    break;
                case MNUSEARCH:
                    await Search(context);
                    break;
                case MNULOADMESSAGES:
                    await LoadMessages(context);
                    break;
                case MNUSENDMESSAGES:
                    await SendMessages(context);
                    break;
                case MNUADDFILE:
                    AddFile(context);
                    break;
            }

        }

        private void AddFile(ObjectsViewContext context)
        {
            var obj = context.SelectedObjects.FirstOrDefault();
            if (obj == null)
                return;
            var fileType = _objectsRepository.GetType(SystemTypeNames.PROJECT_FILE);
            var builder = _objectModifier.Create(obj.Id, fileType);
            builder.AddFile(@"D:\1.pdf");
            builder.SetAttribute(SystemAttributeNames.PROJECT_ITEM_NAME, "1.pdf");
            _objectModifier.Apply();
        }

        private async Task SendMessages(ObjectsViewContext context)
        {
            var message = _messageBuilder.CreateMessage(MessageType.TextMessage, context.SelectedObjects.First().Id)
                .AddText("Message from plugin")
                .Message;
            await _messagesRepository.SendMessageAsync(message);
        }

        private async Task LoadMessages(ObjectsViewContext context)
        {
            var chat = await _messagesRepository.LoadChatAsync(context.SelectedObjects.First().Id);
            var messages = await _messagesRepository.LoadMessagesAsync(chat.Id, DateTime.MinValue.ToUniversalTime(), DateTime.MaxValue.ToUniversalTime(), int.MaxValue);
            foreach (var message in messages)
            {
                if (message.Type != MessageType.TextMessage)
                    continue;
                var data = message.GetData<ITextMessageData>().Text;
            }
        }

        private async Task Search(ObjectsViewContext context)
        {

            var builder = _searchService.GetEmptyQueryBuilder();
            builder.Must(ObjectFields.ObjectState.Be(ObjectState.Alive))
                .Must(ObjectFields.TypeId.Be(_objectsRepository.GetType("ecm_doc").Id))
                .Must(AttributeFields.String("name").Be("Test"))
                .InContext(context.SelectedObjects.First().Id);
            var result = await _searchService.Search(builder).FirstAsync(x => x.Kind == SearchResultKind.Remote);
        }

        private void GetFile(ObjectsViewContext context)
        {
            var obj = context.SelectedObjects.First();
            var snapshot = obj.ActualFileSnapshot;
            var file = snapshot.Files.FirstOrDefault();
            if (file == null)
                return;
            using (var stream = _fileProvider.OpenRead(file))
            {
                var buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                var path = @"D:\test.png";
                System.IO.File.WriteAllBytes(path, buffer);
            }
        }

        private void Edit(ObjectsViewContext context)
        {
            if (context.SelectedObjects.Count() != 2)
                return;
            var first = context.SelectedObjects.First();
            var second = context.SelectedObjects.Last();
            var relationId = Guid.NewGuid();
            var realationName = "relationName";
            var relationType = ObjectRelationType.Custom;
            var one = new Relation { Id = relationId, Name = realationName, Type = relationType, TargetId = first.Id };
            var two = new Relation { Id = relationId, Name = realationName, Type = relationType, TargetId = second.Id };
            _objectModifier.CreateLink(one, two);
            var builder = _objectModifier.EditById(one.Id);
            builder.CreateFileSnapshot(null);
            builder.AddFile(@"C:\Users\ФроловКирилл\Pictures\Screenshots\Screenshot 2023-07-08 103910.png");
            _objectModifier.Apply();
        }

        private void Create(ObjectsViewContext context)
        {
            if (context.SelectedObjects.Count() != 1)
                return;
            var obj = context.SelectedObjects.First();
            if (obj.Type.Name != "project")
                return;
            var ecmType = _objectsRepository.GetType("ecm_doc");
            var newObjId = Guid.NewGuid();
            var builder = _objectModifier.CreateById(newObjId, context.SelectedObjects.First().Id, ecmType);
            builder.SetAttribute("name", "Test");
            builder.SetAttribute("notExists", "111");
            builder.SetAttributeAsObject("project", new string[] { obj.Id.ToString() });
            builder.AddFile(@"C:\Users\ФроловКирилл\Pictures\Screenshots\Screenshot 2023-07-08 103910.png");

            _objectModifier.Apply();
        }

        public void Build(IToolbarBuilder builder, ObjectsViewContext context)
        {
            builder.AddMenuButtonItem("miTaskMenuTest", builder.Count)
                .WithHint("Test")
                .WithShowHeader(true)
                .WithHeader("Test");
        }

        public void OnToolbarItemClick(string name, ObjectsViewContext context)
        {

        }
    }
}
