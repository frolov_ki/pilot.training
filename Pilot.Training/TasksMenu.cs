﻿using Ascon.Pilot.SDK.Menu;
using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Ascon.Pilot.SDK.Toolbar;

namespace Pilot.Training
{
    [Export(typeof(IMenu<TasksViewContext2>))]
    [Export(typeof(IToolbar<TasksViewContext2>))]
    internal class TasksMenu : IMenu<TasksViewContext2>, IToolbar<TasksViewContext2>
    {
        private readonly IObjectsRepository _objectsRepository;
        private bool _isChecked;

        [ImportingConstructor]
        public TasksMenu(IObjectsRepository objectsRepository)
        {
            _objectsRepository = objectsRepository;
        }

        public void Build(IMenuBuilder builder, TasksViewContext2 context)
        {
            builder.AddCheckableItem("miTaskMenuTest", 0)
                .WithIsChecked(_isChecked)
                .WithHeader("Test");
            
        }

        public void Build(IToolbarBuilder builder, TasksViewContext2 context)
        {
            builder.AddMenuButtonItem("miTaskMenuTest", builder.Count)
                .WithHint("Test")
                .WithShowHeader(true)
                .WithHeader("Test");
                
        }

        public void OnMenuItemClick(string name, TasksViewContext2 context)
        {
            _isChecked = !_isChecked;
        }

        public void OnToolbarItemClick(string name, TasksViewContext2 context)
        {
            
        }
    }
}
