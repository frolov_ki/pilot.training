﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Pilot.Training
{
    public interface IAsyncMethods
    {
        Task<IList<IDataObject>> GetObjectsAsync(IObjectsRepository repository, IEnumerable<Guid> ids, CancellationToken ct);
    }

    public static class AsyncMethodsExtensions
    {
        public static IAsyncMethods AsyncMethods = new ReactiveAsyncMethods();

        /// <summary>
        /// Returns objects by identifiers as an asynchronous operation
        /// </summary>
        /// <param name="repository">repository</param>
        /// <param name="ids">Object identifiers to be loaded</param>
        /// <param name="ct">CancellationToken to cancel objects loading. The OperationCanceledException will be thrown.</param>
        /// <returns>The task object representing the asynchronous operation</returns>
        public static Task<IList<IDataObject>> GetObjectsAsync(this IObjectsRepository repository, IEnumerable<Guid> ids, CancellationToken ct)
        {
            return AsyncMethods.GetObjectsAsync(repository, ids, ct);
        }

        public static async Task<IDataObject> GetObjectAsync(this IObjectsRepository repository, Guid id, CancellationToken ct)
        {
            var result = await GetObjectsAsync(repository, new[] { id }, ct);
            return result.FirstOrDefault();
        }
    }

    class ReactiveAsyncMethods : IAsyncMethods
    {
        public async Task<IList<IDataObject>> GetObjectsAsync(IObjectsRepository repository, IEnumerable<Guid> ids, CancellationToken ct)
        {
            // Creates an observable that fires notification when cancelling CancellationToken
            var cancel = Observable.Create<IDataObject>(o => ct.Register(o.OnCompleted));

            var loading = ids.ToList();
            return await repository
                .SubscribeObjects(loading)                          // Subscribing on interested objects
                .TakeUntil(cancel)                                  // Stopping subscription on cancel
                .ObserveOnDispatcher(DispatcherPriority.Background) // Forcing notifications to be raised on UI thread with Background priority
                .Where(o => o.State == DataState.Loaded)   // Filtering "NoData" notifications
                .Distinct(o => o.Id)                       // Filtering already emitted notifications
                .Take(loading.Count)                                // Wait for all objects to be loaded
                .Where(x => repository
                .GetCurrentAccess(x.Id, repository.GetCurrentPerson().Id)
                .HasFlag(AccessLevel.View))                         // Filter non-accessible objects
                .ToList()
                .ToTask(ct);
        }
    }
}
