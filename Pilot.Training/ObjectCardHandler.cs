﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.ObjectCard;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training
{
    [Export(typeof(IObjectCardHandler))]
    internal class ObjectCardHandler : IObjectCardHandler
    {
        [ImportingConstructor]
        public ObjectCardHandler()
        {
                
        }

        public bool Handle(IAttributeModifier modifier, ObjectCardContext context)
        {
           return true;
        }

        public bool OnValueChanged(IAttribute sender, AttributeValueChangedEventArgs args, IAttributeModifier modifier)
        {
            //modifier.SetValue(sender.Name, (string)args.OldValue);
            return true; 
        }
    }
}
