﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training
{
    //[Export(typeof(IObjectChangeProcessor))]
    internal class ObjectChangeProcessor : IObjectChangeProcessor
    {
        [ImportingConstructor]
        public ObjectChangeProcessor()
        {
            
        }

        public bool ProcessChanges(IEnumerable<DataObjectChange> changes, IObjectModifier modifier)
        {
            var builder = modifier.EditById(changes.First().New.Id);
            builder.SetAttribute("name", "name from plugin");
            //modifier.Apply(); не нужно вызывать метод Apply
            return true;
        }
    }
}
