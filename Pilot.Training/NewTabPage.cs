﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training
{
    [Export(typeof(INewTabPage))]
    internal class NewTabPage : INewTabPage
    {
        private readonly ITabServiceProvider _tabServiceProvider;

        [ImportingConstructor]
        public NewTabPage(ITabServiceProvider tabServiceProvider)
        {
            _tabServiceProvider = tabServiceProvider;
        }

        public void BuildNewTabPage(INewTabPageHost host)
        {
            var groupId = Guid.NewGuid();
            host.SetGroup("My group", groupId);
            host.AddButtonToGroup("New Tab", "newTab", "new tab tooltip", null, groupId);
            host.AddButtonToGroup("New Tab2", "newTab2", "new tab tooltip2", null, groupId);
            //host.AddButton("New Tab", "newTab", "new tab tooltip", null);
        }

        public void OnButtonClick(string name)
        {
            //_tabServiceProvider.ShowElement(new Guid("56ca7129-40cc-41f7-88da-b739146d402d"));

            if (name == "newTab")
            {
                _tabServiceProvider.UpdateActiveTabPageContent(new NewTabView());
                _tabServiceProvider.UpdateActiveTabPageTitle("My tab title");
               //var id =  _tabServiceProvider.OpenTabPageAndGetId("My tab title", new NewTabView());
            }
           
        }
    }
}
