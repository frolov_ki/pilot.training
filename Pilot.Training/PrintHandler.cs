﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training
{
    [Export(typeof(IPrintHandler))]
    internal class PrintHandler : IPrintHandler
    {
        public bool Handle(IPrintedDocumentInfo printedDocInfo)
        {
            return true;
        }
    }
}
