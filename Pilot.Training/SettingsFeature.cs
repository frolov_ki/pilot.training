﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Pilot.Training
{
    [Export(typeof(ISettingsFeature))]
    internal class SettingsFeature : ISettingsFeature
    {
        private FrameworkElement _editor;

        public string Key => "Pilot.Training971C1334-A33A-4FAF-A1A3-B36BB49C4658";

        public string Title => "Pilot training settings";

        public FrameworkElement Editor => _editor;

        public void SetValueProvider(ISettingValueProvider settingValueProvider)
        {
            _editor = new SettingsFeatureView(settingValueProvider);
        }
    }
}
