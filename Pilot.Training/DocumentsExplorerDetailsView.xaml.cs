﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.ObjectCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Pilot.Training
{
    /// <summary>
    /// Interaction logic for DocumentsExplorerDetailsView.xaml
    /// </summary>
    public partial class DocumentsExplorerDetailsView : UserControl
    {
        private readonly IObjectCardControlProvider _objectCardControlProvider;
        private readonly IObjectModifier _objectModifier;
        private readonly Guid _id;
        private readonly ObjectCardControl _card;

        public DocumentsExplorerDetailsView(
            IObjectCardControlProvider objectCardControlProvider,
            IObjectModifier objectModifier,
            Guid id)
        {
            InitializeComponent();
            _objectCardControlProvider = objectCardControlProvider;
            _objectModifier = objectModifier;
            _id = id;
            _card  = _objectCardControlProvider.GetObjectCardControl(id, false);
            PilotContentPresenter.Content = _card.View;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!_card.Interaction.GetIsValidInput())
                return;
            
            var values = _card.Interaction.GetValues();
            var builder = _objectModifier.EditById(_id);
            foreach (var value in values)
            {
                builder.SetAttributeAsObject(value.Key, value.Value.Value);
            }
            _objectModifier.Apply();
        }
    }
}
