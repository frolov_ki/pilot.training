﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training
{
    //[Export(typeof(IAutoimportHandler))]
    internal class AutoimportHandler : IAutoimportHandler
    {
        public bool Handle(string filePath, string sourceFilePath, AutoimportSource autoimportSource)
        {
            return false;
        }
    }
}
