﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training
{
    [Export(typeof(IDataPlugin))]
    internal class DataPlugin : IDataPlugin, IObserver<IDataObject>, IObserver<INotification>, IObserver<KeyValuePair<string, string>>
    {
        private readonly IObjectsRepository _objectsRepository;
        private readonly IObjectModifier _objectModifier;
        private readonly IPersonalSettings _personalSettings;
        private readonly IDisposable _subscription;

        [ImportingConstructor]
        public DataPlugin(
            IObjectsRepository objectsRepository,
            IObjectModifier objectModifier,
            IPersonalSettings personalSettings

            )
        {
            _objectsRepository = objectsRepository;
            _objectModifier = objectModifier;
            _personalSettings = personalSettings;
            _personalSettings.SubscribeSetting("Pilot.Training971C1334-A33A-4FAF-A1A3-B36BB49C4658").Subscribe(this);
            _subscription = _objectsRepository.SubscribeObjects(new[] { new Guid("903d8ab3-ba9c-4cca-8a9e-e527fbaeaaaa") }).Subscribe(this);
            _objectsRepository.SubscribeNotification(NotificationKind.ObjectCreated).Subscribe(this);
        }


        public async void OnNext(IDataObject value)
        {
            _subscription?.Dispose();
            var obj = await _objectsRepository.GetObjectAsync(new Guid("903d8ab3-ba9c-4cca-8a9e-e527fbaeaaaa"), System.Threading.CancellationToken.None);
        }

        public void OnCompleted()
        {

        }

        public void OnError(Exception error)
        {

        }

        public void OnNext(INotification value)
        {
            if (value.ChangeKind != NotificationKind.ObjectCreated)
                return;
            var objId = value.ObjectId;
        }

        public void OnNext(KeyValuePair<string, string> value)
        {
            
        }
    }
}
