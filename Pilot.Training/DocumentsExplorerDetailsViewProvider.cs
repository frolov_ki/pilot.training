﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.ObjectCard;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Pilot.Training
{
    [Export(typeof(IDocumentsExplorerDetailsViewProvider))]
    internal class DocumentsExplorerDetailsViewProvider : IDocumentsExplorerDetailsViewProvider
    {
        private readonly IObjectsRepository _objectsRepository;
        private readonly IObjectModifier _objectModifier;
        private readonly IObjectCardControlProvider _objectCardControlProvider;

        [ImportingConstructor]
        public DocumentsExplorerDetailsViewProvider(
            IObjectsRepository objectsRepository,
            IObjectModifier objectModifier,
            IObjectCardControlProvider objectCardControlProvider)
        {
            _objectsRepository = objectsRepository;
            _objectModifier = objectModifier;
            _objectCardControlProvider = objectCardControlProvider;
            Types = new List<IType> { _objectsRepository.GetType("ecm_doc") };
        }

        public List<IType> Types { get; }

        public FrameworkElement GetDetailsView(ObjectsViewContext context)
        {
            return new DocumentsExplorerDetailsView(_objectCardControlProvider, _objectModifier, context.SelectedObjects.First().Id);
        }
    }
}
