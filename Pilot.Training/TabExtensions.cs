﻿using Ascon.Pilot.SDK.Tabs;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training
{
    [Export(typeof(ITabsExtension<DocumentExplorerDetailsTabsContext>))]
    internal class TabExtensions : ITabsExtension<DocumentExplorerDetailsTabsContext>
    {
        public void BuildTabs(ITabsBuilder builder, DocumentExplorerDetailsTabsContext context)
        {
            builder.AddTab(Guid.NewGuid(), "Потомки", new DocumentExplorerDetailsTabView());
        }

        public void OnIsActiveChanged(Guid tabId, bool isActive)
        {

        }

        public void OnDisposed(Guid tabId)
        {
            
        }

       
    }
}
