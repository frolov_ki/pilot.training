﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training
{
    [Export(typeof(INotificationsHandler))]
    internal class NotificationsHandler : INotificationsHandler
    {
        public bool Handle(INotification notification)
        {
            return true;
        }
    }
}
