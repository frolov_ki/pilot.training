﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training
{
    [Export(typeof(IObjectChangeHandler))]
    internal class ObjectChangeHandler : IObjectChangeHandler
    {
        private readonly IObjectsRepository _objectsRepository;
        private readonly IObjectModifier _objectModifier;

        [ImportingConstructor]
        public ObjectChangeHandler(
            IObjectsRepository objectsRepository,
            IObjectModifier objectModifier)
        {
            _objectsRepository = objectsRepository;
            _objectModifier = objectModifier;
        }

        public void OnChanged(IEnumerable<DataObjectChange> changes)
        {
            //Так делать не стоит 
            //var obj = changes.First().New;
            //var builder = _objectModifier.EditById(obj.Id);
            //builder.SetAttribute("name", "New name");
            //_objectModifier.Apply();
        }
    }
}
