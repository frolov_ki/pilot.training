﻿using Ascon.Pilot.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilot.Training
{
    //[Export(typeof(IFileHandler))]
    internal class FileHandler : IFileHandler
    {
        public bool Handle(string inputFile, out List<string> outputFiles, FileHandlerSource source)
        {
            outputFiles = new List<string> { @"C:\Users\ФроловКирилл\Pictures\Screenshots\1.png" };
            return true;
        }
    }
}
