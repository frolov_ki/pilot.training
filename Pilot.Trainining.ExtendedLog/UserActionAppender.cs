﻿using Ascon.Pilot.Common;
using Ascon.Pilot.DataModifier;
using Ascon.Pilot.ServerExtensions.SDK;
using System;
using System.ComponentModel.Composition;

namespace Pilot.Trainining.ExtendedLog
{
    [Export(typeof(IUserActionAppender))]
    public class UserActionAppender : IUserActionAppender
    {
        public string ProcessEvent(IModifierBackend backend, IBaseEventContext context)
        {
            if (context is IChangesetContext changesetContext)
            {

                var obj =  backend.GetObject(changesetContext.ObjectId);
                var type  = backend.GetType(obj.TypeId);
                return $"{obj.GetTitle(type)}  - from UserActionAppender";
            }
            return null;
        }
    }
}
